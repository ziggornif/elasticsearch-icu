# elasticsearch-icu

Elasticsearch image with analysis-icu plugin

## Pulling the image

```sh
docker pull ziggornif/elasticsearch-icu:7.13.0
```

## Start a single node

```sh
docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" ziggornif/elasticsearch-icu:7.13.0
```

## Docker-comose example

```yaml
version: "3.9"
services:
  elastic:
    image: ziggornif/elasticsearch-icu:7.13.0
    container_name: elastic
    environment:
      - cluster.name=docker-cluster
      - discovery.type=single-node
      - ES_JAVA_OPTS=-Xms512m -Xmx512m
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - es-data:/usr/share/elasticsearch/data
    ports:
      - 9200:9200
      - 9300:9300
volumes:
  es-data:
    driver: local
```